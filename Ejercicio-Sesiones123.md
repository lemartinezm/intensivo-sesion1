# Intensivo Sesión 1

## Dependencias instaladas:

### Dependencias generales:
* Express: utilizada para crear de forma rápida y sencilla una API. En la sesión se utilizó para la creación de la ruta '/' (página principal) y la devolución (método GET) de un string.
* Dotenv: utilizada para la carga de variables de entorno desde el archivo .env creado.

### Dependencias de desarrollo:
* ESLINT: es una herrmienta utilizada para inspección del código y la aplicación de ciertas normas definidas. En la sesión se escogió las normas estándar.
* TypeScript: super-conjunto de JavaScript. Usado para mejorar el tipado débil que tiene JavaScript.
* @types/express: contiene las definiciones de tipos de express.
* @types/jest: contiene las definiciones de tipos de jest.
* @types/node: contiene las definiciones de tipos de node.
* Nodemon: permite ejecutar de manera automática el archivo index.js cuando detecta un cambio en el código. Utilizado para la ejecución del script "dev".
* Concurrently: permite la ejecución de varios comandos a la vez. Utilizado para la ejecución del script "dev".
* Jest: utilizado para realizar testing.
* Supertest: usado para realizar testing HTTP.
* Serve: utilizado para la creación de aplicaciones que contienen una sola página o un sitio estático. Se usa para lanzar el coverage que devuelte el testing con Jest.
* Webpack: empaquetador de módulos. Utilizado para brindar archivos JavaScript para su uso en el navegador.
* Webpack-CLI: brinda una serie de comandos útiles para realizar una configuración mas rápida de un proyecto Webpack customizado.
* Webpack-node-externals: permite definir los elementos que no deben ser empaquetados por Webpack.
* Webpack-shell-plugin: permite ejecutar un comando shell antes o después del empaquetamiento.
* TS-Node: utilizado

## Scripts:
* build: ejecuta la transpilación del archivo index.ts y lo guarda en la carpeta ./dist.
* start: ejecuta el archivo creado en la carpeta ./dist mediante el comando build.
* dev: ejecuta dos comandos. El primero observa cambios hechos en el archivo index.ts y ejecuta la transpilación cada vez que detecte alguno. El segundo ejecuta el archivo creado por la tranpilación con nodemon.
* test: ejecuta los test definidos en la carpeta __test__.
* serve:coverage: ejecuta el testing con Jest, luego cambia a la carpeta creada 'coverage/lcov-report' y ejecuta el comando 'npx serve' para lanzar el coverage como un html.

## Variables de entorno:
* PORT: el puerto donde se ejecutará el servidor.


# Intensivo Sesión 2
Controladores y rutas.

## Dependencias instaladas:

### Dependencias generales:
* cors: agrega seguridad mediante CORS. Permite elegir desde que dominios se puede realizar peticiones a la API.
* helmet: agrega seguridad agregando HTTP headers.

# Sesión 3
Empaquetamiento con Webpack, documentación con Swagger-TSOA e introducción a MongoDB y preparación de archivos ts.

## Dependencias instaladas:

### Dependencias generales:
* swagger-jsdoc
* swagger-ui-express
* mongoose

### Dependencias de desarrollo:
* ts-loader
* @types/swagger-jsdoc
* @types/swagger-ui-express

## Scripts:
Se modificaron y crearon algunos scripts.
* swagger: para generar el archivo JSON que contiene la documentación sobre la aplicación creado utilizando tsoa.
* dev: se agregó el comando swagger creado.
* build: se modificó para que ahora el script de construcción utilice webpack para empaquetar en un único archivo js (modo desarrollo).
* build:prod: creación del script para empaquetar en modo producción (mejor tiempo de carga que el modo desarrollo)
import dotenv from 'dotenv'
import server from './src/server'
import { LogError, LogSuccess } from './src/utils/Logger'

// Read .env file
dotenv.config()

// App creation
const port: string | number = process.env.PORT || 8000

// Deploy
server.listen(port, () => {
  LogSuccess(`[SERVER ON]: Running in http://localhost:${port}`)
})

// Server error
server.on('error', (error) => {
  LogError(`[SERVER ERROR]: ${error}`)
})

# Sesión 1

## Comandos
* node index.js: ejecuta el index (servidor).
* npx tsc --init: crea el tsconfig.json con las configuraciones recomendadas en el directorio actual.
* npx tsc: ejecuta la transpilación del código TypeScript.
* npx eslint --init: crear el archivo de configuración de eslint.
* npx jest --init

# Sesión 2

## Pasos
1. Crear en '.src/controllers': interfaces y types.
    * Interface vs Type: un type no puede ser modificado después de haber sido creado. Para ver mas información: https://www.typescriptlang.org/docs/handbook/2/everyday-types.html#differences-between-type-aliases-and-interfaces
    * Crear los index.ts de interfaces y types. En interface se crea la interface IHelloController que será implementada por la clase HelloController. En types se crean los types BasicResponse y ErrorResponse utilizados para Promise<'type'> de IHelloController.
    * Crear HelloController.ts, crear la clase HelloController e implementar la interfaz IHelloController. Es necesario tipar la función como async (https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/async_function#descripci%C3%B3n) porque el retorno está tipado como Promise.
2. Dentro de utils, crear el archivo Logger. Este archivo tendrá las funciones que imprime por consola los diferentes tipos de logs (LogSuccess, LogError, LogInfo, LogWarning).
    * Incluir el LogSuccess en HelloController
3. Crear el router para HelloController.
    * Crear helloRouter.
    * Importar express para poder utilizar express.Router(). ¿Por qué express.Router() y no solo express() como en otros casos? Ver esto: https://stackoverflow.com/questions/28305120/differences-between-express-router-and-app-get
    * Definir la ruta para el método GET (por ahora). Dentro contiene una función async 
4. Crear el index en routes. Este archivo es que el configura todos los enrutados (contiene el rootRouter y las redirecciones como por ejemplo el HelloRouter)
5. Crear el index en server. Este archivo gestiona la configuración de seguridad (cors, helmet) y redirecciones.
    * Instalar cors y helmet en dependencias generales e importarlas. Probablemente pida instalar tambien los types de cors (@types/cors)
6. Completar la configuración del archivo index general del proyecto. Importar server desde server y usarlo para la escucha en el puerto definido en las variables de ambiente. Ademas agregar un control de errores.

# Sesión 3
Empaquetamiento con Webpack, documentación con Swagger-TSOA e introducción a MongoDB y preparación de archivos ts.
## Comandos
* mongod --dbpath {acá va donde quieres guardar los documentos}
* mongo: para ejecutar el mongo shell

## Pasos
1. Configuración de webpack mediante la creación del archivo webpack.config.js.
    * Será necesario instalar el ts-loader.
    * Añadir los scripts necesarios para el empaquetamiento (para el build de producción y desarrollo y el start)
2. Instalar Swagger(swagger-jsdoc, swagger-ui-express y sus types) y TSOA.
    * Crear el archivo de configuración tsoa.json.
    * Realizar la documentación de los controladores (puede pedir que asignar a true el experimentalDecorators del tsconfig)
    * Agregar el script para obtener el json Swagger y agregar este script al script dev.
3. Crear la base de datos con MongoDB en la terminal.
    * Instalar mongoose, agregar a domain las entidades necesarias y agregar el esquema que deberá tener los documentos.
    * Agregar el método get para las entidades en orm.
    * Crear los repositorios (configuración inicial)

# Sesión 4
Corrección del UI Swagger (agregado la configuración necesaria para servir el JSON en ./docs) y práctica de comandos de búsqueda de MongoDB en el Mongo Shell.

## Comandos para Mongo Shell:
* help
* cls: limpiar ventana
* show dbs: mostrar las DB disponibles
* use {nombre DB}
* show collections
* db.Users.find({name: "Luis"}).pretty(): para ver mejor el JSON
* db.{collection}.insertOne()
* db.{collection}.insertMany([{},{},{}])
* db.{collection}.help

# Sesión 5
Implementación de CRUD

## Pasos:
1. Implementar getUsers(): todas las peticiones CRUD serán manejadas por el orm. Tener cuidado con las funciones asíncronas y sus devoluciones (async y awaits xd).
    * Creamos la interfaz IUserController
    * Crear el UserController: para devolver todos los usuarios, utilizar getAllUsers() implementado anteriormente en User.orm.ts).
    * Crear el userRouter: acá se implementarán todos los routes para los diferentes métodos (get, post, etc)
    * Agregar userRouter al index.ts de routes.
    * No olvidar la documentación.
2. Agregar la búsqueda por ID mediante los Query Params.
    * Agregar el método de búsqueda por id en user.orm.ts.
    * Modificar getUsers para que acepte ID mediante query params (esto se agrega en el UserRouter)
3. Implementar deleteUser()
    * Agregar a la interface deleteUser()
    * Modificar el UserController y agregar el método deleteUser
    * Agregar al UserRouter el método delete.
    * Agregar al orm el método para borrar por id
4. Implementar createUser()
    * Agregar a la interface createUser()
    * Modificar el UserController y agregar el método createUser()
    * Agregar al UserRouter el método post.
    * Agregar al orm el método para crear por id
5. Implementar updateUser()

# Sesión 6
Implementación inicial para el uso de JWT

## Pasos
1. Crear la interface a usar para el userSchema nuevo que reemplazará al UserEntity
    * IUser: interface que contiene la estructura del Schema.
    * Notas sobre pruebas: si tratas de crear un nuevo usuario sin completar todos los campos, arroja error (he modificado para que se le muestre al usuario un mensaje de confirmación o error). Es posible pedir un documento al cual le falte un campo (se hizo la prueba eliminando el campo age en un documento)
2. Actualizar los códigos de estado HTTP ( res.status({estado}).send(response) )
    * Get documents => 200 OK
    * Creation Documents => 201 OK
    * Deletion of documents => 200 (Entity) / 204(No return)
    * Update of documents => 200 (Entity) / 204(No return)
    * 400 Bad Request
3. Falta configurar las entradas por body
4. Instalar las dependencias necesarias (prod) y sus types (dev).
    * jsonwebtoken
    * bcrypt
    * bcryptjs
5. Crear el middleware en 'src' (se ejecuta antes de realizar una operación. En nuestro caso, será intermedio de verificación para el acceso a los datos requeridos)
6. Crear verifyToken.middleware.ts y su función para verificar el token que vendrá en las cabeceras.
7. Agregar el password a la interface IUser.
8. Modificar el ORM y agregar métodos de login, register y logout.
    * Para register se utiliza la interface IUser.
    * Para login se utiliza la interface IAuth.
9. Crear el AuthController (junto con su interface IAuthController) que gestionará el registro, login y logout de usuarios.
    * Crear el método de registro (tener en cuenta que la contraseña debe encriptarse antes de ser guardada).
    * Nota: Las Queries que devuelve los métodos del ORM (.find() por ejemplo), no son promesas. https://mongoosejs.com/docs/queries.html (Queries are not promises) 
10. Crear el enrutado para las peticiones de autorización.

# Sesión 7
Configuración de rutas protegidas con el verifyToken middleware.

## Pasos
1. Corregir la entrada de JSON por Body (¿usar body-parser? Express tiene express.json() incluido).
2. Modificar el loginUser en ORM. Incluir la variable SECRET de env (! para especificar que no va a ser indefinido las variables que se quejen).
3. Crear una ruta que utilice el Middleware de verificación JWT (obtención de datos mediante ID).
4. Agregar la verificación JWT a todos los métodos de Users. Eliminar las creaciones de /api/users, ahora solo serán manejadas por /api/auth/register

# Sesión 8
Paginación y configuración de la respuesta mostrada al cliente (por ejemplo: no enviar la contraseña).

## Pasos:
1. Modificar el getAllUsers para que incluya paginación. En el return del ORM debe contener los usuarios, página actual y el número de páginas totales.

# Sesión 9
Gestión de relaciones entre las katas y los users.

## Pasos:
1. Creación de todo lo necesario para realizar peticiones CRUD a la colección Katas de la base de datos.
    * ORM
    * Router
    * Controller
2. Modificar el kataSchema con lo dado en clase. Agregar la interface IKata en la creación del schema.
3. Agregar al userSchema sus katas creadas. Modificar sus peticiones CRUD en lo que sea necesario para agregar sus katas como un array.
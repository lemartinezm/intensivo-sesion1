import mongoose from 'mongoose'

/**
 * Interface containing name, email, password, age, katas and role
 */
export interface IUser {
  name: string,
  email: string,
  password: string,
  age: number,
  katas: string[],
  role: string
}

/**
 * Interface containing _id, name, email, password, age, katas and role
 */
export interface IUserFound {
  _id: mongoose.Schema.Types.ObjectId
  name: string,
  email: string,
  password: string,
  age: number,
  katas: string[],
  role: string
}

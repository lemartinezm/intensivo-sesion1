import mongoose from 'mongoose'
import { IUser } from '../interfaces/IUser.interface'

export const userEntity = () => {
  // Esquema antiguo que fue reemplazado por el de abajo
  // const userSchema = new mongoose.Schema(
  //   {
  //     name: String,
  //     email: String,
  //     age: Number
  //   }
  // )

  // Especifica que el esquema es de tipo IUser
  const userSchema = new mongoose.Schema<IUser>(
    {
      name: { type: String, required: true },
      email: { type: String, required: true },
      password: { type: String, required: true },
      age: { type: Number, required: true },
      katas: { type: [], required: true },
      role: { type: String, required: true }
    }
  )
  return mongoose.models.Users || mongoose.model<IUser>('Users', userSchema)
}
// Cuidado con el return de modelos, al ejecutar por primera vez funciona, pero luego es necesario utilizar mongoose.models.{nombre del modelo creado} para evitar problemas de sobre escritura.

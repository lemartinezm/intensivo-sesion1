import { Get, Query, Route, Tags } from 'tsoa'
import { LogSuccess } from '../utils/Logger'
import { IHelloController } from './interfaces'
import { BasicResponse } from './types'

@Route('/api/hello')
@Tags('HelloController')
export class HelloController implements IHelloController {
  // se debe agregar async (ver el md)
  /**
   * Método para obtener un mensaje "Hello {nombre}" en formato JSON
   * @param { string | undefined} name  Nombre para saludar
   * @returns { BasicResponse } Promesa de BasicResponse
   */
  @Get('/')
  async getMessage (@Query()name?: string): Promise<BasicResponse> {
    LogSuccess('[/api/hello] Get Request')
    return {
      message: `Hello ${name || 'anónimo'}`
    }
  }
}

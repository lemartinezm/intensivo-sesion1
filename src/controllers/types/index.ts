
/**
 * Response básica JSON para controllers
 */
export interface BasicResponse {
  message: string
}

/**
 * Error response JSON para controllers
 */
export type ErrorResponse = {
  message: string
}

/**
 * Goodbye response JSON para GoodbyeController
 */
export type GoodbyeResponse = {
  message: string,
  date: Date
}

export type Filter = {
  level?: string,
  language?: number
}

export type Sort = {
  name?: number,
  description?: number,
  level?: number,
  user?: number,
  date?: number,
  valoration?: number,
  numValorations?: number,
  allValorations?: number,
  chances?: number
}

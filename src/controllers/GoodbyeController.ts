import { Get, Route, Query, Tags } from 'tsoa'
import { LogSuccess } from '../utils/Logger'
import { IGoodbyeController } from './interfaces'
import { GoodbyeResponse } from './types'

@Route('/api/goodbye')
@Tags('GoodbyeController')
export class GoodbyeController implements IGoodbyeController {
  /**
   * Método para devolver el mensaje "Goodbye {nombre}"
   * @param { string | undefined} name Nombre a despedir
   * @returns { GoodbyeResponse } Promesa de tipo GoodbyeResponse
   */
  @Get('/')
  public async getMessage (@Query()name?: string): Promise<GoodbyeResponse> {
    LogSuccess('Método GET GoodbyeController')
    const date: Date = new Date()
    return {
      message: `Goodbye ${name || 'anónimo'}`,
      date: date
    }
  }
}

import { Post, Route, Tags, Body } from 'tsoa'
import { IAuth } from '../domain/interfaces/IAuth.interface'
import { IUser } from '../domain/interfaces/IUser.interface'
import { loginUser, registerUser } from '../domain/orm/Auth.orm'
import { LogInfo } from '../utils/Logger'
import { IAuthController } from './interfaces'

@Route('/api/auth')
@Tags('AuthController')
export class AuthController implements IAuthController {
  /**
   * Endpoint to register new User
   * @param user User Data to register (name + email + password + age + katas + role)
   * @returns Object with response status and confirmation or error message
   */
  @Post('/register')
  public async registerUsers (@Body()user: IUser): Promise<any> {
    LogInfo('[/api/auth/register] Creating new user')
    return await registerUser(user)
  }

  /**
   * Endpoint to login
   * @param auth User data to login (email + password)
   * @returns Object with response status, User found and Token signed or response status and error message
   */
  @Post('/login')
  public async loginUsers (@Body()auth: IAuth): Promise<any> {
    LogInfo(`[/api/auth/login] Trying to log: ${auth.email}`)
    return await loginUser(auth)
  }
}

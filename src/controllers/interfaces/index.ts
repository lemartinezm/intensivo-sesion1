import { IAuth } from '../../domain/interfaces/IAuth.interface'
import { IUser } from '../../domain/interfaces/IUser.interface'
import { BasicResponse, GoodbyeResponse } from '../types'

export interface IHelloController {
  // name? (podría o no aparecer): tipado
  getMessage(name?: string): Promise<BasicResponse>
  //  La función debería retornar una Promise tipo BasicResponse
}

export interface IGoodbyeController {
  getMessage(name?: string): Promise<GoodbyeResponse>
}

export interface IUserController {
  getUsers(page: number, limit: number, id?: string): Promise <any>
  deleteUser(id?: string): Promise <any>
  updateUsers(id: string, user: any): Promise <any>
  getKatas(page: number, limit: number, id: string): Promise <any>
}

export interface IKataController {
  getKatas(loggedUserId: string, page?: number, limit?: number, id?: string, filter?: any, sortType?: string): Promise <any>
  deleteKata(id: string, loggedUserId: string, isAdmin: boolean): Promise <any>
  createKatas(kata: any, loggedUserId: string): Promise <any>
  updateKatas(id: string, loggedUserId: string, isAdmin: boolean, kata: any): Promise <any>
}

export interface IAuthController {
  registerUsers(user: IUser): Promise <any>
  loginUsers(auth: IAuth): Promise <any>
}

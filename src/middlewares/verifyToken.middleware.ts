import { NextFunction, Request, Response } from 'express'
import jwt from 'jsonwebtoken'

// Variables de entorno
import dotenv from 'dotenv'
dotenv.config()
const SECRET = process.env.SECRETKEY

/**
 * Token verifier
 * @param req Petición original previo a la verificación del token
 * @param res Respuesta de la verificación del JWT
 * @param next Siguiente función que será ejecutada
 * @returns Siguiente ejecución o error de verificación
 */
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  // Verificar los headers en busca del 'x-access-token'
  const token: any = req.headers['x-access-token']

  // Verificar si el JWT está presente
  if (!token) {
    return res.status(403).send({
      authenticationError: 'Missing JWT in request',
      message: 'Not authorized to use this endpoint'
    })
  }

  // Verificar el jwt obtenido
  jwt.verify(token, SECRET!, (err: any, decoded: any) => {
    if (err) {
      return res.status(500).send({
        authenticationError: 'JWT verification failed',
        message: 'Failed to verify JWT in request'
      })
    }
    // Guarda el id del usuario loggeado y lo pasa a la siguiente función utilizando res.locals
    res.locals.userId = decoded.id
    // Execute Next Function -> Protected routes will be executed
    next()
  })
}

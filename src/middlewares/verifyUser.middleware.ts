import { NextFunction, Request, Response } from 'express'
import { hasRepeatedEmail, hasRepeatedName } from '../domain/orm/Auth.orm'
import { LogError } from '../utils/Logger'

/**
 * Verify if already exists the username or the email that the user is trying to register with
 * @param req Original request that user sent
 * @param res Response of the request
 * @param next Next function that will be executed
 * @returns Next execution or error message
 */
export const verifyUser = async (req: Request, res: Response, next: NextFunction) => {
  const name: any = req?.body?.name
  const email: any = req?.body?.email
  let isRepeated: boolean = false

  isRepeated = await hasRepeatedName(name)

  if (isRepeated) {
    LogError('[Register]: Username is already registered')
    return res.status(400).send({
      message: 'Username is already registered. Please use another username.'
    })
  }

  isRepeated = await hasRepeatedEmail(email)

  if (isRepeated) {
    LogError('[Register]: Email is already registered')
    return res.status(400).send({
      message: 'Email is already registered.'
    })
  }

  next()
}

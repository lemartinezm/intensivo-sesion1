import express, { Request, Response } from 'express'
import { HelloController } from '../controllers/HelloController'
import { LogInfo } from '../utils/Logger'

const helloRouter = express.Router()

helloRouter.route('/')
  .get(async (req: Request, res: Response) => {
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)
    const controller: HelloController = new HelloController()
    const response = await controller.getMessage(name)
    return res.send(response)
  })

export default helloRouter

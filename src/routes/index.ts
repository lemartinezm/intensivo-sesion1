/**
 * Root Router
 */

import express, { Request, Response } from 'express'
import authRouter from './AuthRouter'
import goodbyeRouter from './GoodbyeRouter'
import helloRouter from './HelloRouter'
import kataRouter from './KataRouter'
import userRouter from './UserRouter'
import cors from 'cors'
import helmet from 'helmet'

const server = express()

const rootRouter = express.Router()

// Security
server.use(cors())
server.use(helmet())

rootRouter.get('/', (req: Request, res: Response) => {
  res.send('¡Hola!, esta es mi API Restful.')
})

// Redirecciones
server.use('/', rootRouter)
server.use('/hello', helloRouter)
server.use('/goodbye', goodbyeRouter)
server.use('/users', userRouter)
server.use('/katas', kataRouter)
server.use('/auth', authRouter)

export default server

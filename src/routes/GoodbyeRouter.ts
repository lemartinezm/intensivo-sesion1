import express, { Request, Response } from 'express'
import { GoodbyeController } from '../controllers/GoodbyeController'
import { LogInfo } from '../utils/Logger'

const goodbyeRouter = express.Router()

goodbyeRouter.route('/')
  .get(async (req: Request, res: Response) => {
    const name: any = req?.query?.name
    LogInfo(`Query Param: ${name}`)
    const controller = new GoodbyeController()
    const response = await controller.getMessage(name)
    return res.send(response)
  })

export default goodbyeRouter

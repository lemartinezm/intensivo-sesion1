import express, { Express, Request, Response } from 'express'

// Swagger
import swaggerUi from 'swagger-ui-express'

// Root Router
import rootRouter from '../routes'
import mongoose from 'mongoose'

// Variables de entorno
import dotenv from 'dotenv'
dotenv.config()
const ATLASPASS = process.env.AtlasPassword

// Creación de la app
const server: Express = express()

// Configuración de Swagger
server.use(
  '/docs',
  swaggerUi.serve,
  swaggerUi.setup(
    undefined, {
      swaggerOptions: {
        url: '/swagger.json',
        explorer: true
      }
    }
  )
)

// Indica el router para la dirección /api
server.use('/api', rootRouter)

// Servidor estático
server.use(express.static('public'))

// Conexión con MongoDB
// mongoose.connect('mongodb://localhost:27017/codeverification')

// Para deploy MongoAtlas
mongoose.connect(`mongodb+srv://intensivoDB123:${ATLASPASS}@cluster0.etc3u.mongodb.net/codeverification?retryWrites=true&w=majority`)

// Content type Config
server.use(express.urlencoded({ extended: true, limit: '50mb' }))
server.use(express.json({ limit: '50mb' }))

// Redireccion a api

server.get('/', (req: Request, res: Response) => {
  res.redirect('/api')
})

export default server

# Consultas utilizando Mongo Compass y Mongo Shell

## Mongo Compass:
* Listar todos los contactos:
    * Filter: {}

* Busca el primer contacto que sea de Alemania (Germany):
    * Filter: {"location.country": "Germany"}
    * Limit: 1

* Busca todos los contactos que tengan Blake como nombre (first):
    * Filter: {"name.first": "Blake"}

* Busca los primeros 5 contactos que tengan como género (gender) hombre (male):
    * Filter: {gender: "male"}
    * Limit: 5

* Devuelve los 4 primeros contactos ordenados por nombre (name) de manera descendente:
    * Sort: {"name.first": -1}
    * Limit: 4

* Clona la colección de Contacts a CopiaContacts y luego bórrala: en la pestaña aggregations agregar lo siguiente:
    * Para clonar:
        * En el primer recuadro seleccionar $match y eliminar la palabra query que aparecerá.
        * Hacer click en Add stage y aparecerá otro recuadro.
        * En este recuadro seleccionar $out y escribir 'copiaContacts'.
    * Para eliminar:
        * En la parte izquierda, buscar la base de datos y dentro la colección copiaContacts. Hacer click en los tres puntos y seleccionar Drop Collection

* Renombra el campo de name por nombre:
    * No es posible actualizar un campo de todos los archivos en MongoDB Compass, ya que, para actualizar un documento, Compass utiliza la operación findOneAndUpdate o findOneAndReplace.
    * Se podría utilizar Mongosh (se encuentra en la parte inferior de Compass) y se tendría que ejecutar el comando escrito en la siguiente sección.

* Borra todos los contactos que tengan como estado (state) Florida:
    * No es posible eliminar múltiples contactos a la vez. Para realizar esta operación es necesario utilizar Mongo Shell.

## Mongo Shell:
* Listar todos los contactos:
    * db.Contacts.find()

* Busca el primer contacto que sea de Alemania (Germany):
    * db.Contacts.findOne({"location.country": "Germany"})
    * db.Contacts.find({"location.country": "Germany"}).limit(1)

* Busca todos los contactos que tengan Blake como nombre (first):
    * db.Contacts.find({"name.first": "Blake"})

* Busca los primeros 5 contactos que tengan como género (gender) hombre (male):
    * db.Contacts.find({gender: "male"}).limit(5)

* Devuelve los 4 primeros contactos ordenados por nombre (name) de manera descendente:
    * db.Contacts.find().sort({"name.first": -1}).limit(4)

* Clona la colección de Contacts a CopiaContacts y luego bórrala:
    * Para clonar la colección:
        * db.Contacts.aggregate([ { $match: {} }, { $out: "copiaContacts"} ])
    * Para eliminar la colección copiaContacts:
        * db.copiaContacts.drop()

* Renombra el campo de name por nombre:
    * db.Contacts.updateMany({}, {$rename: {name: "nombre"}})

* Borra todos los contactos que tengan como estado (state) Florida:
    * db.Contacts.deleteMany({"location.state": "Florida"})
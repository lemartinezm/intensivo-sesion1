# Consultas utilizando Mongo Compass y Mongo Shell

## Mongo Compass:

* Muestra las primeras 5 ciudades que empiecen por A ordenadas de manera ascendente, las soluciones deben ser únicas: se realizó mediante agregaciones.
    * $group: {_id: 0, city: {$addToSet: "$location.city"}}
    * $unwind: 'city'
    * $match: {city: /^A/}
    * $sort: {city: 1}
    * $limit: 5

* Crea una colección a parte, que solo contenga a los contactos de Francia (France) y que tengan entre 18 y 50 años. Usa una agregación para ello.
    * $match: {"location.country": "France", "dob.age": {$gte: 18, $lte: 50}}
    * $out: 'France1850'

* Añade un número favorito a cada contacto, luego crea un bucket agrupando por número favorito que separe en 5 segmentos.
    * No es posible agregar un número a cada contacto mediante Mongo Compass. Es necesario realizarlo mediante forEach con Mongo Shell.
    * $bucketAuto: {groupBy: "$favNumber", buckets: 5 }

* En la colección de Contacts, haz una proyección la cual tiene que devolver solo el name y username del contacto.
    * project: {name: 1, "login.username": 1}

* Haz una consulta en la colección de Contacts la cual devuelva un documento por cada nombre (name) y que sea único, ordenado por apellido (last), tienes que usar el operador $unwind.
    * $group: {_id: 0, name: {$addToSet: "$name"}}
    * $unwind: "$name"
    * $sort: {"name.last": 1}

* Haz una proyección convirtiendo la fecha (date) a un formato DD-MM-AAAA, la nueva variable será fechaNacimiento
    * $project: { "fechaNacimiento": {$concat: [{$substrBytes: ["$dob.date", 8,2]}, "-", {$substrBytes: ["$dob.date", 5, 2]}, "-", {$substrBytes: ["$dob.date", 0, 4]} ]}}

## Mongo Shell:

* Muestra las primeras 5 ciudades que empiecen por A ordenadas de manera ascendente, las soluciones deben ser únicas:
    * db.Contacts.aggregate([ {$group: {_id:0, city: {$addToSet: "$location.city"} } }, {$unwind: '$city'}, {$match: {city: /^A/}}, {$sort: {city: 1}}, {$limit: 5} ])

* Crea una colección a parte, que solo contenga a los contactos de Francia (France) y que tengan entre 18 y 50 años. Usa una agregación para ello.
    * db.Contacts.aggregate([ {$match: {"location.country": "France", "dob.age": {$gte: 18, $lte: 50}}}, {$out: 'France1850'} ])

* Añade un número favorito a cada contacto, luego crea un bucket agrupando por número favorito que separe en 5 segmentos.
    * db.Contacts.find().forEach((contact) => {db.Contacts.update({_id: contact._id}, {$set: {"favNumber": Math.floor(Math.random() * 500) }})})
    * db.Contacts.aggregate([ {$bucketAuto: {groupBy: "$favNumber", buckets: 5 }} ])

* En la colección de Contacts, haz una proyección la cual tiene que devolver solo el name y username del contacto.
    * db.Contacts.find({}, {name: 1, "login.username": 1})

* Haz una consulta en la colección de Contacts la cual devuelva un documento por cada nombre (name) y que sea único, ordenado por apellido (last), tienes que usar el operador $unwind.
    * db.Contacts.aggregate([ {$group: {_id: 0, name: {$addToSet: "$name"}}}, {$unwind: "$name"}, {$sort: {"name.last": 1}} ])

* Haz una proyección convirtiendo la fecha (date) a un formato DD-MM-AAAA, la nueva variable será fechaNacimiento
    * db.Contacts.aggregate([ {$project: { "fechaNacimiento": {$concat: [{$substrBytes: ["$dob.date", 8,2]}, "-", {$substrBytes: ["$dob.date", 5, 2]}, "-", {$substrBytes: ["$dob.date", 0, 4]} ]} }} ])
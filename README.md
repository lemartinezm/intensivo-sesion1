# Open Katas Backend
Open Katas Backend is an API that handles database requests. The API has implemented a user registration and login option. In addition, it is possible to perform CRUD operations on users and katas (some functionalities are restricted to users with administrator role).

The project has used TypeScript, NodeJS, Express, Mongoose and some other dependencies that are listed in the package.json file.

Realized as part of the final project proposed by OpenBootcamp in the course "Intensivo Abril - MERN"

## Usage

Clone it:

```
$ git clone https://gitlab.com/lemartinezm/intensivo-sesion1
```

Go into the project directory and run the command:
```
npm install
```
For run in dev mode:
```
npm run dev
```
For other scripts, check the package.json file.

## API Preview
https://intensivo-back.onrender.com

## API Documentation
https://intensivo-back.onrender.com/docs

## License
[MIT](LICENSE.txt)